using System.Collections.Generic;
using Domain.Model.Base;

namespace Domain.Services.Interface.Base
{
    public interface IServiceBase
    {
        Entidade Buscar(long id);
        IEnumerable<Entidade> Buscar(int skip, int take);
    }
}