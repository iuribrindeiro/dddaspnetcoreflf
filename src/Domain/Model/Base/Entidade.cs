using System;

namespace Domain.Model.Base
{
    public abstract class Entidade {
        public long Id { get; set; }
        public DateTime DataCriacao { get; set; }
        public DateTime DataAtualizacao { get; set; }
    }
}